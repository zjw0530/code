package  com.zjw.entity;

public class Nterprisetalent {
  private int id;
  private String name;
  private int workinglife;
  private String workexperience;
  private String personalprofile;
  private int departmentid;
  private String graduateschool;

  private Aepartment aepartment;

  public Aepartment getAepartment() {
    return aepartment;
  }

  public void setAepartment(Aepartment aepartment) {
    this.aepartment = aepartment;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getWorkinglife() {
    return workinglife;
  }

  public void setWorkinglife(int workinglife) {
    this.workinglife = workinglife;
  }

  public String getWorkexperience() {
    return workexperience;
  }

  public void setWorkexperience(String workexperience) {
    this.workexperience = workexperience;
  }

  public String getPersonalprofile() {
    return personalprofile;
  }

  public void setPersonalprofile(String personalprofile) {
    this.personalprofile = personalprofile;
  }

  public int getDepartmentid() {
    return departmentid;
  }

  public void setDepartmentid(int departmentid) {
    this.departmentid = departmentid;
  }

  public String getGraduateschool() {
    return graduateschool;
  }

  public void setGraduateschool(String graduateschool) {
    this.graduateschool = graduateschool;
  }
}
