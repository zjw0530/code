package com.zjw.dao;

import com.zjw.entity.Nterprisetalent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NterprisetalentDao {

    /**
     * 查询全部信息
     *
     * @return
     */

    List<Nterprisetalent> queryAirqualityindex(@Param("departmentId") String departmentId, @Param("pageNo") int pageNo, @Param("pageSize")int pageSize);

    /**
     * 查询总行数
     * @return
     */
    int queryRows(@Param("departmentId") String departmentId);


    /**
     * 添加图书
     * @return
     */
    int insertInvitation(Nterprisetalent nterprisetalent );

    /**
     * 根据id显示信息
     * @param id
     * @return
     */
    Nterprisetalent showAirById(int id);

    /**
     * 根据id修改信息
     * @param nterprisetalent
     * @return
     */
    int updateAir(Nterprisetalent nterprisetalent);





    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    int deleteAir(int id);

}
