package com.zjw.dao;

import com.zjw.entity.Aepartment;

import java.util.List;

public interface AepartmentDao {

    /**
     * 查询全部区域
     * @return
     */
    List<Aepartment> queryDistrict();
}

