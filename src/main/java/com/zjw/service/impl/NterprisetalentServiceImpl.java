package com.zjw.service.impl;

import com.zjw.dao.NterprisetalentDao;
import com.zjw.entity.Nterprisetalent;
import com.zjw.entity.Pager;
import com.zjw.service.NterprisetalentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NterprisetalentServiceImpl implements NterprisetalentService {
    @Resource
    private NterprisetalentDao nterprisetalentDao;

    public Pager<Nterprisetalent> queryAirqualityindex(String departmentId, int pageNo, int pageSize) {
        Pager pager = new Pager();
        pager.setPageNo(pageNo);
        pager.setPageSize(pageSize);
        pager.setTotalRows(nterprisetalentDao.queryRows(departmentId));
        pager.setTotalPage((pager.getTotalRows() + pageSize - 1) / pageSize);
        pager.setDatas( nterprisetalentDao.queryAirqualityindex(departmentId,(pageNo - 1) * 4,pageSize));
        return pager;
    }

    public boolean insertInvitation(Nterprisetalent nterprisetalent) {
        return nterprisetalentDao.insertInvitation(nterprisetalent)>0;
    }

    public boolean deleteAir(int id) {
        return nterprisetalentDao.deleteAir(id)>0;
    }

    public Nterprisetalent showAirById(int id) {
        return nterprisetalentDao.showAirById(id);
    }

    public boolean updateAir(Nterprisetalent nterprisetalent) {
        return nterprisetalentDao.updateAir(nterprisetalent)>0;
    }
}
