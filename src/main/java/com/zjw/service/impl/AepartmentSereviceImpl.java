package com.zjw.service.impl;

import com.zjw.dao.AepartmentDao;
import com.zjw.entity.Aepartment;
import com.zjw.service.AepartmentSerevice;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AepartmentSereviceImpl implements AepartmentSerevice {
    @Resource
    private AepartmentDao aepartmentDao;

    public List<Aepartment> queryDistrict() {
        return aepartmentDao.queryDistrict();
    }
}
