package com.zjw.service;

import com.zjw.entity.Nterprisetalent;
import com.zjw.entity.Pager;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NterprisetalentService {
    /**
     * 查询全部信息
     *
     * @return
     */
    Pager<Nterprisetalent> queryAirqualityindex(String departmentId, int pageNo, int pageSize);

    /**
     * 添加图书
     * @return
     */
    boolean insertInvitation(Nterprisetalent nterprisetalent );

    /**
     * 根据id删除信息
     * @param id
     * @return
     */
    boolean deleteAir(int id);

    /**
     * 根据id显示信息
     * @param id
     * @return
     */
    Nterprisetalent showAirById(int id);


    /**
     * 根据id修改信息
     * @param nterprisetalent
     * @return
     */
    boolean updateAir(Nterprisetalent nterprisetalent);
}
