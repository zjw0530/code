package com.zjw.service;

import com.zjw.entity.Aepartment;

import java.util.List;

public interface AepartmentSerevice {
    /**
     * 查询全部区域
     * @return
     */
    List<Aepartment> queryDistrict();
}
