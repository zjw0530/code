package com.zjw.controller;

import com.zjw.dao.AepartmentDao;
import com.zjw.entity.Aepartment;
import com.zjw.entity.Nterprisetalent;
import com.zjw.entity.Pager;
import com.zjw.service.AepartmentSerevice;
import com.zjw.service.NterprisetalentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class NterprisetalentController {

    @Resource
    private NterprisetalentService nterprisetalentService;
    @Resource
    private AepartmentSerevice aepartmentSerevice;

    @RequestMapping("/query")
    public String queryAirqualityindex(String departmentId,String pageNo, Model model) {
        if ("".equals(pageNo) || null == pageNo) {
            pageNo = "1";
        }
        Pager<Nterprisetalent> nterprisetalents = nterprisetalentService.queryAirqualityindex(departmentId,Integer.parseInt(pageNo),4);
        List<Aepartment> aepartmentList = aepartmentSerevice.queryDistrict();
        model.addAttribute("nterprisetalents", nterprisetalents);
        model.addAttribute("aepartmentList", aepartmentList);
        model.addAttribute("departmentId", departmentId);
        return "index";
    }

    @RequestMapping("/redirectAdd")
    public String redirectAdd(int departmentId, Model model) {
        model.addAttribute("departmentId", departmentId);
        List<Aepartment> aepartmentList = aepartmentSerevice.queryDistrict();
        model.addAttribute("aepartmentList", aepartmentList);

        return "insert";
    }

    @RequestMapping("/insertAri")
    public String insertAir(Nterprisetalent Nterprisetalent) {
        if (nterprisetalentService.insertInvitation(Nterprisetalent)) {
            return "redirect:query";
        }
        return "";
    }

    @RequestMapping("/deleteAir")
    public String deleteAir( int id) {
        nterprisetalentService.deleteAir(id);
        return "redirect:query";
    }

    @RequestMapping("/showAirById/{id}")
    public String showAir(@PathVariable("id") int id, Model model) {
        Nterprisetalent nterprisetalent = nterprisetalentService.showAirById(id);
        model.addAttribute("nterprisetalent", nterprisetalent);
        List<Aepartment> aepartmentList = aepartmentSerevice.queryDistrict();
        model.addAttribute("aepartmentList", aepartmentList);

        return "showAir";
    }
    @RequestMapping(value = "/updateAir")
    public String updateAir(Nterprisetalent Nterprisetalent, Model model) {
        nterprisetalentService.updateAir(Nterprisetalent);
//        model.addAttribute("air",air);
        return "redirect:query";
    }
    @RequestMapping(value = "/updateAir")
    public String zjw(Nterprisetalent Nterprisetalent, Model model) {
        return "redirect:query";
    }
    @RequestMapping(value = "/aaa")
    public String aaa(Nterprisetalent Nterprisetalent, Model model) {
        return "redirect:query";
    }

    @RequestMapping(value = "/bbb")
    public String bbb(Nterprisetalent Nterprisetalent, Model model) {
        return "redirect:query";
    }
}
