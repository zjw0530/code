<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>
<body>

<form action="/insertAri" method="post">
    <input type="hidden" name="departmentId" id="" value="${departmentId}">
    <table align="center" border="1px" style="width: 500px">
        <tr>
            <td colspan="2" align="center" height="50px" style="background-color: green;font-size: 25px">增加图书</td>
        </tr>
        <tr>
            <td>人才姓名</td>
            <td><input type="text" name="name" id=""></td>
        </tr>
        <tr>
            <td>工作年限</td>
            <td>  <input type="text" name="workinglife" id="">
           </td>
        </tr>
        <tr>
            <td>所属部门</td>
            <td>
                <select name="departmentid">
                    <c:if test="${ aepartmentList  != null}">
                        <option value="0">请选择</option>
                        <c:forEach items="${aepartmentList}" var="dist">
                            <option  value="${dist.id}">${dist.name} </option>
                        </c:forEach>
                    </c:if>
                </select>
            </td>
        </tr>
        <tr>
            <td>毕业学校</td>
            <td><input type="text" name="graduateschool" id=""></td>
        </tr>
        <tr>
            <td>个人简介</td>
            <td>
                <textarea name="personalprofile" id="" cols="30" rows="10"></textarea>
        </tr>
        <tr>
            <td>工作经历</td>
            <td>
                <textarea name="workexperience" id="" cols="30" rows="10"></textarea>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="提交">
                <input type="button" value="返回" onclick="javascript:history.back(-1);">
            </td>
        </tr>
    </table>
</form>

</body>
</html>
