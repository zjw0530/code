<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

   <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
</head>
<body>
<h1 align="center">人才信息系列表</h1>


<form action="/query" method="get" id="formId">

    <table align="center" border="1px" width="600px">
        <tr>
            <td colspan="6" align="center">
                按区域查找<select name="departmentId">
                <c:if test="${ aepartmentList  != null}">
                    <option value="0">请选择</option>
                    <c:forEach items="${aepartmentList}" var="dist">
                        <option
                                <c:if test="${dist.id==departmentId}">selected="selected"</c:if>
                                value="${dist.id}">${dist.name} </option>
                    </c:forEach>
                </c:if>
            </select><input type="submit" value="查询">
                <a href="/redirectAdd?departmentId=${departmentId}">添加电子图书</a>
            </td>
        </tr>
        <tr>
            <td>序号</td>
            <td>人才姓名</td>
            <td>工作年限</td>
            <td>所属部门</td>
            <td>毕业学校</td>
            <td>操作</td>
        </tr>
        <c:forEach items="${nterprisetalents.datas}" var="air">
            <tr>
                <td>${air.id}</td>
                <td>${air.name}</td>
                <td>${air.workinglife}</td>
                <td>${air.aepartment.name}</td>
                <td>${air.graduateschool}</td>

                <td><a href="/showAirById/${air.id}">修改</a>
                    <a href="/deleteAir?id=${air.id}">删除</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</form>
<p align="center">
    <c:if test="${nterprisetalents.pageNo>1}">
        <a href="/query?pageNo=1&departmentId=${departmentId}">首页</a>
        <a href="/query?pageNo=${nterprisetalents.pageNo-1}&departmentId=${departmentId}">上一页</a>
    </c:if>
    <c:if test="${nterprisetalents.pageNo<nterprisetalents.totalPage}">
        <a href="/query?pageNo=${nterprisetalents.pageNo+1}&departmentId=${departmentId}">下一页</a>
        <a href="/query?pageNo=${nterprisetalents.totalPage}&departmentId=${departmentId}">尾页</a>
    </c:if>
    <c:if test="${nterprisetalents.totalPage>0}">
        <a href="javascript:;">第${nterprisetalents.pageNo}页/共${nterprisetalents.totalPage}页</a>
    </c:if>
</p>
</body>
</html>
